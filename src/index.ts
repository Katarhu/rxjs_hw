import './style.css';

import {
    Observable,
    of,
    tap,
    delay,
    catchError,
    mergeMap,
    map,
    filter,
    from,
    toArray,
    fromEvent,
    switchMap,
    takeUntil,
    interval, take, takeWhile, share, reduce, switchMapTo
} from 'rxjs';
  import { ajax } from 'rxjs/ajax';

const $button = document.querySelector('button');
const $input = document.querySelector('input');
const $body = document.querySelector('body');

// Створити Observable, яка буде віддавати 2 синхронні значення "1", "2", а через 2 секунди викидувати помилку. Ваша задача використовуючи існуючі оператори обробити цю помилку всередині pipe, для того, щоб вона не дійшла до subscribe

// const stream$ = new Observable((subscriber) => {
//     subscriber.next(1);
//     subscriber.next(2);
//     setTimeout(() => subscriber.error('Error after 2sec'), 2000);
// }).pipe(
//     catchError(err => {
//         console.log('Error caught: ' + err);
//         return of();
//     }))
// ;
//
// stream$.subscribe({
//     next: console.log,
//     error: console.log,
//     complete: () => console.log('Completed')
// });


// Створити аналог fromEvent оператора( який під капотом використовує addEventListener).
// Не забувайте про витоки пам'яті і те, як їх уникати в RxJS(після відписання від цього оператора ми не повинні більше слухати події)


// function myFromEvent(element: HTMLElement, eventAction: string) {
//     return new Observable((subscriber) => {
//         const callback = (e: Event) => {
//             subscriber.next(e);
//         }
//
//         element.addEventListener(eventAction, callback);
//
//         return function unsubscribe() {
//             console.log('unsubscribed');
//             element.removeEventListener(eventAction, callback);
//         }
//     }).pipe(
//         map((val: PointerEvent) => ({ x: val.x, y: val.y, target: val.target}))
//     )
// }
//
// const buttonStream$ = myFromEvent($button, 'click');
// const buttonSubscription = buttonStream$.subscribe({
//     next: console.log,
//     error: console.log
// })
//
// setTimeout(() => {
//     buttonSubscription.unsubscribe()
// }, 3500);

// Використовуючи оператор interval, підписатися на нього і слухати до того моменту, доки значення не буде більше 5(використовуючи оператор в pipe)

// const intervalStream$ = interval(1000).pipe(
//     takeWhile(val => val < 5)
// )
//
// intervalStream$.subscribe({
//     next: console.log
// })

// Перетворіть coldInterval нижче на hotInterval, щоб він став гарячим(віддавав одні і ті ж значення різним підписникам)
// Приклад:
// sub1 subscribed
// sub1 0
// sub1 1
// sub2 subscribed
// sub1 2
// sub2 2
// sub1 3
// sub2 3

// const coldInterval$ = interval(1000)
//
// coldInterval$.subscribe((val) => console.log('First subscriber: ' + val))
//
// setTimeout(() => {
//     coldInterval$.subscribe((val) => console.log('Second subscriber: ' + val))
// }, 2000)


// let hotInterval$ =
//     interval(1000).pipe(
//         take(10),
//         share()
//     );
//
//
// hotInterval$.subscribe({
//     next: (val) => console.log('First subscriber: ' + val),
//     error: console.log,
//     complete: () => console.log('Completed for 1')
// })
//
// setTimeout(() => {
//     hotInterval$.subscribe({
//         next: (val) => console.log('Second subscriber: ' + val),
//         error: console.log,
//         complete: () => console.log('Completed for 2')
//     })
// }, 3000)


// Обробити відповідь запиту, в pipe спочатку витягнути об'єкт response(це масив), відфільтруєте масив так, щоб залишилися тільки пости з id менше 5.
// Hint: так як response - це буде масив постів, ви не можете просто фідфільтрувати його через filter(він приймає кожен елемент масиву, а не цілий масив). Для рішення цієї задачі вам потрібні оператори mergeMap або concatMap, в яких ви зробите з(перекладіть англійською) масиву потік окремих елементів масиву([1, 2, 3] => 1, 2, 3), відфільтруєте їх,а потім зберете назад в масив за допомогою оператора. В subscribe ми отримаємо масив з 4 об'єктів id яких менше 5

// interface IPost {
//     userId: number
//     id: number,
//     title: string
//     body: string
// }
//
//
// const ajax$ = ajax.getJSON('https://jsonplaceholder.typicode.com/posts')
//     .pipe(
//         switchMap((val: IPost[]) => from(val)),
//         filter((val: IPost) => val.id < 5),
//         reduce((acc, val) => acc.concat(val), [])
//     )
//
// ajax$.subscribe({
//     next: (val) => console.log(val)
// })

// Використовуючи Rxjs написати потік, який буде слухати кліки по кнопці і відправляти при натисканню на неї запит на сервер із текстом введеним в пошук. В subscribe ми маємо отримати дані з серверу.
// Оператори, які можуть знадобитися: fromEvent, switchMap, ajax, map, etc

// const buttonStream$ = fromEvent($button, 'click').pipe()
// const URL = 'https://jsonplaceholder.typicode.com/comments?postId='
//
// buttonStream$.subscribe({
//     next: (val) => {
//         ajax.getJSON(URL + $input.value)
//             .subscribe((val) => console.log(val))
//     }
// })



// Використовуючи RxJs зробити свою імплементацію Drag&Drop.
// Деталі: Створіть 3 observable mousedown$, mousemove$, mouseup$. Які будуть слухати події mousedown, mousemove, mouseup відповідно. Ваша задача поєднати їх так, щоб mousemove$ починав працювати тільки коли користувач натикає  на mousedown, і переставали слухати, коли відбувається mouseup. Тобто постійно ви маєте слухати тільки mousedown, а підписуватися на зміну mousemove i mouseup тільки після івенту mousedown
// const mousedown$ = ... .pipe().subscribe(value - колекція mousemove подій, яка починається віддаватися при mousedown і закінчує стрім при mouseup)



// const mouseup$ = fromEvent($body, 'mouseup')
//     .pipe(
//         tap((val) => console.log(val.type))
//     )
// const mousemove$ = fromEvent($body, 'mousemove')
//     .pipe(
//         takeUntil(mouseup$),
//         tap((val) => console.log(val.type)),
//     );
// const mousedown$ = fromEvent($body, 'mousedown')
//     .pipe(
//         tap(val => console.log(val.type)),
//         switchMap(() =>
//             mousemove$
//         )
//     )
// ;
//
// mousedown$.subscribe()
